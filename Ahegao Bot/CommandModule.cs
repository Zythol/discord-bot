﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

using Discord.Commands;
using Discord.WebSocket;
using Discord;

using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints.Impl;
using Imgur.API.Models.Impl;
using Imgur.API.Enums;

namespace Ahegao_Bot.Services
{
    public class CommandModule
    {
        private string listFilePath = Directory.GetCurrentDirectory() + "//Level_50_list.txt";

        public async Task CommandSearcher(SocketCommandContext context, string funcName, string parameter)
        {
            Type thisType = this.GetType();
            MethodInfo theMethod = thisType.GetMethod(funcName);
            var result = theMethod.Invoke(this, new object[] { context, parameter });
        }

        #region general

        public async Task PrintCommands(SocketCommandContext context, CommandWord commandWord)
        {
            string replyMessage = string.Empty;
            List<EmbedFieldBuilder> ListOfFieldBuilder = new List<EmbedFieldBuilder>();

            EmbedBuilder embed = new Discord.EmbedBuilder();
            embed.WithColor(new Discord.Color(255, 251, 38));

            embed.WithTitle("Ahgao Bot help page");
            embed.WithDescription("Command usage: ```ah!<command>```");
            //Build the commands message
            foreach (CommandWord cWord in commandWord.commandWord)
            {
                ListOfFieldBuilder.AddRange(cWord.GetHelpInfoRecur(""));
                cWord.ClearList();
                commandWord.ClearList();
            }

            if(ListOfFieldBuilder.Count > 0)
            {
                foreach(EmbedFieldBuilder fieldBuilder in ListOfFieldBuilder)
                {
                    embed.AddField(fieldBuilder);
                }
            }
            //Refer the user to private message
            await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```Please refer to the private message HawkGirl has sent you.```");
            //Send user private message
            await context.User.SendMessageAsync("", false, embed);

            //Clear list
            ListOfFieldBuilder.Clear();
        }

        // ~say hello -> hello
        public async Task EchoPrint(SocketCommandContext context, string param)
        {
            await context.Channel.SendMessageAsync(param);
        }

        public async Task SayForMe(SocketCommandContext context, string param)
        {
            await context.Message.DeleteAsync();
            await context.Channel.SendMessageAsync(param);
        }

        public async Task SayForMeTTS(SocketCommandContext context, string param)
        {
            await context.Message.DeleteAsync();
            await context.Channel.SendMessageAsync(param, true);
        }

        public async Task RandomImgur(SocketCommandContext context, string param)
        {

        }

        #endregion

        #region FiftyList
        public async Task AddToFiftyList(SocketCommandContext context, string user)
        {
            if (!File.Exists(listFilePath))
            {
                //Creates the text file and close the stream
                var fs = File.CreateText(listFilePath);
                fs.Close();
            }

            //Append the log to the file
            using (StreamWriter sw = File.AppendText(listFilePath))
            {
                await sw.WriteAsync(user + ",");
                string replyMessage = " " + user + " has been added into the list!";
                await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + replyMessage + "```");
            }
        }

        public async Task ShowFiftyList(SocketCommandContext context, string param)
        {
            if (!File.Exists(listFilePath))
            {
                await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```List file not found. Please add a user to create the file.```");
            }
            else
            {
                int mod = 0;
                string contentString = File.ReadAllText(listFilePath);
                string[] arrayOfUser = contentString.Split(',');
                string description = string.Empty;
                EmbedBuilder embed = new EmbedBuilder();
                EmbedFieldBuilder fieldBuilder = new EmbedFieldBuilder();

                embed.WithTitle("Level Fifty List");
                for (int i = 1; i < arrayOfUser.Length; i++)
                {
                    mod = i % 5;
                    if (mod == 0)
                    {
                        description += " | " + arrayOfUser[i - 1] + "```";
                    }
                    else if (mod == 1)
                    {
                        description += "```" + arrayOfUser[i - 1];
                    }
                    else if (mod >= 2)
                    {
                        description += " | " + arrayOfUser[i - 1];
                    }
                }
                //Check if we need to close off the coded box
                if (mod != 0)
                {
                    description += "```";
                }

                embed.WithDescription(description);
                await context.Channel.SendMessageAsync(context.Message.Author.Mention, false, embed);
            }
        }

        public async Task DeleteUserFiftyList(SocketCommandContext context, string user)
        {
            if (!File.Exists(listFilePath))
            {
                await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```List file not found. Please add a user to create the file.```");
            }
            else
            {
                string contentString = File.ReadAllText(listFilePath);
                List<string> listOfUser = contentString.Split(',').ToList<string>();
                bool deleted = listOfUser.Remove(user);

                //Rebuild the string
                contentString = string.Empty;

                //Remove all empty strings
                listOfUser.RemoveAll(item => item == "");

                if (deleted)
                {
                    string[] arrayOfUser = listOfUser.ToArray();
                    foreach (string users in arrayOfUser)
                    {
                        contentString += users + ",";
                    }

                    await File.WriteAllTextAsync(listFilePath, contentString);
                    await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + user + " has been removed from the list.```");
                }
                else
                {
                    await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + user + " not found!```");
                }
            }
        }

        public async Task SearchFiftyList(SocketCommandContext context, string param)
        {
            if (!File.Exists(listFilePath))
            {
                await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```List file not found. Please add a user to create the file.```");
            }
            else
            {
                string contentString = File.ReadAllText(listFilePath);
                List<string> listOfUser = contentString.Split(',').ToList<string>();
                if(listOfUser.Contains(param))
                {
                    await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + param + " is in the list.```");
                }
                else
                {
                    await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + param + " is not the list.```");
                }
            }
        }
        #endregion

        #region Alarm

        public async Task AddReminder(SocketCommandContext context, string param)
        {
            //hour mins title message
            //year month day hour mins title message
            List<string> paramList = param.Split(' ', 7).ToList<string>();
            string replyMessage = string.Empty;


            AlarmInfo temp = new AlarmInfo();
            temp.isDaily = "false";
            temp.realAlarm = new DateTime(Convert.ToInt32(paramList[0]), Convert.ToInt32(paramList[1]), Convert.ToInt32(paramList[2]), Convert.ToInt32(paramList[3]), Convert.ToInt32(paramList[4]), 0);
            temp.name = paramList[5];
            temp.message = paramList[6];
            temp.countDownInterval = new List<int> { 0, 5, 15, 30 };

            Program.services.GetService<Alarm>().alarmList.alarms.Add(temp);

            string serializedAlarm = JsonConvert.SerializeObject(Program.services.GetService<Alarm>().alarmList);

            File.WriteAllText(Program.services.GetService<Alarm>().jsonFilePath, serializedAlarm);

            await Program.services.GetService<Alarm>().Init();

            replyMessage = string.Empty;
            replyMessage = "You've created a one-time reminder at " + temp.realAlarm.ToString();

            await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + replyMessage + "```");
        }

        public async Task AddDailyReminder(SocketCommandContext context, string param)
        {
            List<string> paramList = param.Split(' ').ToList<string>();
            string replyMessage = string.Empty;

            AlarmInfo temp = new AlarmInfo();
            temp.isDaily = "true";
            temp.realAlarm = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, Convert.ToInt32(paramList[0]), Convert.ToInt32(paramList[1]), 0);
            temp.name = paramList[2];
            temp.message = paramList[3];
            temp.countDownInterval = new List<int> { 0, 5, 15, 30 };

            Program.services.GetRequiredService<Alarm>().alarmList.alarms.Add(temp);

            string serializedAlarm = JsonConvert.SerializeObject(Program.services.GetRequiredService<Alarm>().alarmList);

            File.WriteAllText(Program.services.GetRequiredService<Alarm>().jsonFilePath, serializedAlarm);

            await Program.services.GetRequiredService<Alarm>().Init();

            replyMessage = string.Empty;

            replyMessage = "You've created a daily reminder at " + temp.realAlarm.ToString();

            await context.Channel.SendMessageAsync(context.Message.Author.Mention + "```" + replyMessage + "```");
        }

        #endregion

        #region Imgur

        public async Task GetRandomMeme(SocketCommandContext context, string param)
        {
            List<string> listOfLinks = new List<string>();
            ImgurClient imgurClient = new ImgurClient("c8d70f2236356a3", "d3bc8ec6f8425db4d73431ed65851138799d9c59");
            GalleryEndpoint galleryEndpoint = new GalleryEndpoint(imgurClient);

            var memeGallery = await galleryEndpoint.GetMemesSubGalleryAsync();
            var images = memeGallery.OfType<GalleryImage>();
            var galleries = memeGallery.OfType<GalleryAlbum>();

            if (images.Count() > 0)
            {
                foreach (GalleryImage image in images)
                {
                    listOfLinks.Add(image.Link);
                }
            }

            if (galleries.Count() > 0)
            {
                foreach (GalleryAlbum album in galleries)
                {
                    if (album.Images.Count() > 0)
                        foreach (var image in album.Images)
                        {
                            listOfLinks.Add(image.Link);
                        }
                }
            }

            //get a random index
            Random rand = new Random();

            //var image = images.ElementAt(rand.Next(images.Count()));
            await context.Channel.SendMessageAsync(listOfLinks[rand.Next(listOfLinks.Count)]);
        }

        public async Task GetRandomNsfw(SocketCommandContext context, string param)
        {
            List<string> listOfLinks = new List<string>();
            ImgurClient imgurClient = new ImgurClient("c8d70f2236356a3", "d3bc8ec6f8425db4d73431ed65851138799d9c59");
            GalleryEndpoint galleryEndpoint = new GalleryEndpoint(imgurClient);

            var memeGallery = await galleryEndpoint.GetSubredditGalleryAsync("nsfw");
            var images = memeGallery.OfType<GalleryImage>();
            var galleries = memeGallery.OfType<GalleryAlbum>();

            if (images.Count() > 0)
            {
                foreach (GalleryImage image in images)
                {
                    listOfLinks.Add(image.Link);
                }
            }

            if (galleries.Count() > 0)
            {
                foreach (GalleryAlbum album in galleries)
                {
                    if (album.Images.Count() > 0)
                        foreach (var image in album.Images)
                        {
                            if (image.Nsfw == true)
                            {
                                listOfLinks.Add(image.Link);
                            }
                        }
                }
            }

            //get a random index
            Random rand = new Random();

            //var image = images.ElementAt(rand.Next(images.Count()));
            //await context.Channel.SendMessageAsync(listOfLinks[rand.Next(listOfLinks.Count)]);
            await context.Guild.GetTextChannel(404991870226399232).SendMessageAsync(listOfLinks[rand.Next(listOfLinks.Count)]);
        }

        public async Task GetRandomNsfwSelf(SocketCommandContext context, string param)
        {
            List<string> listOfLinks = new List<string>();
            ImgurClient imgurClient = new ImgurClient("c8d70f2236356a3", "d3bc8ec6f8425db4d73431ed65851138799d9c59");
            GalleryEndpoint galleryEndpoint = new GalleryEndpoint(imgurClient);

            var memeGallery = await galleryEndpoint.GetSubredditGalleryAsync("nsfw");
            var images = memeGallery.OfType<GalleryImage>();
            var galleries = memeGallery.OfType<GalleryAlbum>();

            if (images.Count() > 0)
            {
                foreach (GalleryImage image in images)
                {
                    listOfLinks.Add(image.Link);
                }
            }

            if (galleries.Count() > 0)
            {
                foreach (GalleryAlbum album in galleries)
                {
                    if (album.Images.Count() > 0)
                        foreach (var image in album.Images)
                        {
                            if (image.Nsfw == true)
                            {
                                listOfLinks.Add(image.Link);
                            }
                        }
                }
            }

            //get a random index
            Random rand = new Random();

            //var image = images.ElementAt(rand.Next(images.Count()));
            //await context.Channel.SendMessageAsync(listOfLinks[rand.Next(listOfLinks.Count)]);
            await context.User.SendMessageAsync(listOfLinks[rand.Next(listOfLinks.Count)]);
        }

        #endregion
    }
}
