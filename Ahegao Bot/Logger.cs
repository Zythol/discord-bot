﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Discord;
using Discord.WebSocket;
using System.Reflection;

namespace Ahegao_Bot.Services
{
    class Logger
    {
        private DiscordSocketClient _discord;
        private string logFilePath = Directory.GetCurrentDirectory() + "//Ahegao_chat_log.txt";

        public Logger(DiscordSocketClient discordClient)
        {
            _discord = discordClient;
            _discord.MessageReceived += MessageLog;
        }

        public Task MessageLog(SocketMessage message)
        {
            string printMessage = string.Empty;

            if (message.Source == MessageSource.User)
            {
                printMessage = message.Timestamp.ToLocalTime() + "--" + message.Channel + "/" + message.Author.Username + " : " + message.ToString();

                if (!File.Exists(logFilePath))
                {
                    //Creates the text file and close the stream
                    var fs = File.CreateText(logFilePath);
                    fs.Close();
                }

                //Append the log to the file
                using (StreamWriter sw = File.AppendText(logFilePath))
                {
                    sw.WriteLine(printMessage);
                }


                Console.WriteLine(printMessage);
            }
            return Task.CompletedTask;
        }

        public Task Log(LogMessage message)
        {
            Console.WriteLine(message.ToString());
            return Task.CompletedTask;
        }
    }
}
