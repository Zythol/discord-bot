﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

using Discord.WebSocket;
using Discord;
using Newtonsoft.Json.Converters;

namespace Ahegao_Bot.Services
{
    enum AlarmStatus
    {
        TRIGGERALARM = 0,
        ALARMCONTINUES,
        ALARMFINSIHED
    }

    class Alarm
    {
        public string jsonFilePath = Directory.GetCurrentDirectory() + "//AlarmTracker.json";
        public AlarmList alarmList;
        private DiscordSocketClient discordClient;

        public Alarm(DiscordSocketClient discordClient)
        {
            this.discordClient = discordClient;
            discordClient.Connected += Init;
        }

        public Task Init()
        {
            alarmList = JsonConvert.DeserializeObject<AlarmList>(File.ReadAllText(jsonFilePath), new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy HH:mm" });

            if (alarmList != null)
            {
                if (alarmList.alarms.Count > 0)
                {
                    for (int i = 0; i < alarmList.alarms.Count; i++)
                    {
                        alarmList.alarms[i].Init();
                    }

                    Timer alarmTimer = new Timer(1000);
                    alarmTimer.Enabled = true;
                    alarmTimer.Elapsed += AlarmCheck;
                }
            }
            else
            {
                alarmList = new AlarmList();
                alarmList.Init();
            }
            return Task.CompletedTask;
        }

        private void AlarmCheck(object sender, System.EventArgs e)
        {
            if (alarmList.alarms.Count > 0)
            {
                for(int i = 0; i < alarmList.alarms.Count; i++)
                {
                    AlarmInfo alarmInfo = alarmList.alarms[i];
                    ////check if init finished
                    //if(alarmInfo.counter > 0
                    switch (alarmInfo.IsTimeUp())
                    {
                        case AlarmStatus.TRIGGERALARM:
                            EmbedBuilder embed = new EmbedBuilder();
                            embed.WithTitle("Reminder");
                            embed.WithDescription(alarmInfo.countDownInterval[alarmInfo.GetCounter() + 1] + " minutes left till the following event:");

                            EmbedFieldBuilder fieldBuilder = new EmbedFieldBuilder();
                            fieldBuilder.WithName(alarmInfo.name);
                            fieldBuilder.WithValue(alarmInfo.message);

                            embed.AddField(fieldBuilder);
                            discordClient.GetGuild(324409110915448832).GetTextChannel(388181521078550538).SendMessageAsync("", false, embed);
                            break;

                        case AlarmStatus.ALARMFINSIHED:
                            if (alarmInfo.isDaily == "true")
                            {
                                alarmInfo.InitDaily();

                                //We do not want to save the countdownreminder timings in the json file. Let the program
                                //generate whenever it loads again
                                //alarmInfo.countDownReminder.Clear();

                                string serializedAlarm = JsonConvert.SerializeObject(alarmList);

                                File.WriteAllText(jsonFilePath, serializedAlarm);
                            }
                            else
                            {
                                alarmList.alarms.Remove(alarmInfo);

                                string serializedAlarm = JsonConvert.SerializeObject(alarmList);

                                File.WriteAllText(jsonFilePath, serializedAlarm);
                            }
                            break;
                    }
                }
            }
        }
    }

    class AlarmList
    {
        public List<AlarmInfo> alarms { get; set; }

        public void Init()
        {
            if (alarms == null)
                alarms = new List<AlarmInfo>();
        }
    }

    class AlarmInfo
    {
        public string name { get; set; }
        public string message { get; set; }
        public DateTime realAlarm { get; set; }
        public string isDaily { get; set; }

        private int counter = -1;
        public List<int> countDownInterval { get; set; }
        private List<DateTime> countDownReminder = new List<DateTime>();

        public void Init()
        {
            //Take into account zero based
            counter = countDownInterval.Count - 1;
            foreach(int i in countDownInterval)
            {
                DateTime temp = new DateTime();
                temp = realAlarm.Subtract(TimeSpan.FromMinutes(i));
                countDownReminder.Add(temp);
            }
        }

        public AlarmStatus IsTimeUp()
        {
            if(counter >= 0 && countDownReminder.Count > 0)
            {
                if(DateTime.Now > countDownReminder[counter])
                {
                    --counter;
                    return AlarmStatus.TRIGGERALARM;
                }
                else
                {
                    return AlarmStatus.ALARMCONTINUES;
                }
            }
            return AlarmStatus.ALARMFINSIHED;
        }

        public void InitDaily()
        {
            realAlarm = realAlarm.AddDays(1);
            countDownReminder.Clear();
            Init();
        }

        public int GetCounter()
        {
            return counter;
        }
    }
}
