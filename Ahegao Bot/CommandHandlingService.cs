﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Newtonsoft.Json;

namespace Ahegao_Bot.Services
{
    public class CommandHandlingService
    {
        private string jsonFilePath = Directory.GetCurrentDirectory() + "//CommandList.json";
        private DiscordSocketClient discordClient;
        private CommandModule commandModule;
        private CommandWord command;

        public CommandHandlingService(DiscordSocketClient discord)
        {
            discordClient = discord;
            commandModule = new CommandModule();

            command = JsonConvert.DeserializeObject<CommandWord>(File.ReadAllText(jsonFilePath));
        }

        public async Task MessageReceived(SocketMessage rawMessage)
        {
            string commandCheck = string.Empty;
            //string commandGroup = string.Empty;
            string selectedText = string.Empty;
            CommandWord commandWord = command;
            KeyValuePair<CommandWord, string> returnedValues = new KeyValuePair<CommandWord, string>();

            // Ignore system messages and messages from bots
            if (rawMessage.Source == MessageSource.User)
            {
                var message = rawMessage as SocketUserMessage;

                SocketCommandContext context = new SocketCommandContext(discordClient, message);

                int argPos = 0;

                //For ah! prefix
                if (!(message.HasStringPrefix("ah!", ref argPos) || message.HasMentionPrefix(discordClient.CurrentUser, ref argPos)))
                    return;
                else
                {
                    //Get the command line. Remove "ah!"
                    int pos = message.Content.IndexOf('!');
                    commandCheck = message.Content.Substring(pos + 1);

                    if(commandCheck == string.Empty)
                    {
                        await commandModule.PrintCommands(context, command);
                        return;
                    }

                    returnedValues = commandWord.CommandSearch(commandCheck);

                    //Allow if user has perms to run this command or is private message
                    if(HasPerms(context, returnedValues.Key))
                    {
                        await commandModule.CommandSearcher(context, returnedValues.Key.funcName, returnedValues.Value);
                        return;
                    }
                }
            }
        }

        private bool HasPerms(SocketCommandContext context, CommandWord cWord)
        {
            //If a pm, allow all commands
            if(context.IsPrivate)
            {
                return true;
            }
            //if perms is empty, just return true
            else if (cWord.perms.Count > 0)
            {
                var user = context.User as SocketGuildUser;
                var listOfRoles = user.Roles;
                //check if user has perms to run this command
                foreach (SocketRole role in listOfRoles)
                {
                    if (cWord.perms.Contains(role.Name))
                    {
                        return true;
                    }
                }
                return false;
            }
            else
                return true;
        }

    }

    public class CommandWord
    {
        public string name { get; set; }
        public string funcName { get; set; }
        public string descript { get; set; }
        public string parameterHelp { get; set; }
        public List<string> perms { get; set; }

        public List<CommandWord> commandWord { get; set; }
        private List<EmbedFieldBuilder> ListOfFieldBuilder = new List<EmbedFieldBuilder>();

        public void ClearList()
        {
            ListOfFieldBuilder.Clear();
        }

        public KeyValuePair<CommandWord, string> CommandSearch(string message)
        {
            string selectedText = string.Empty;
            int pos = 0;

            //Find which commandWord object does the command belongs to
            foreach (CommandWord cWord in commandWord)
            {
                //Pick out the first command from string
                pos = message.IndexOf(" ");

                //No more characters in the string
                if (pos < 0)
                {
                    selectedText = message;
                }
                else
                {
                    selectedText = message.Substring(0, pos);
                }

                if (cWord.name == selectedText)
                {
                    //Shift on to the remaining string
                    message = message.Substring(pos + 1);
                    //If this is the command itself, return this
                    if (cWord.commandWord.Count == 0)
                    {
                        return new KeyValuePair<CommandWord, string>(cWord, message);
                    }
                    //Else search deeper
                    else
                    {
                        return cWord.CommandSearch(message);
                    }
                }
            }
            return new KeyValuePair<CommandWord, string>();
        }

        public List<EmbedFieldBuilder> GetHelpInfoRecur(string parentName)
        {
            EmbedFieldBuilder fieldBuilder = new EmbedFieldBuilder();

            string passDownName = parentName + " " + name;

            //If is not a function, don't add in
            if (string.IsNullOrEmpty(funcName) == false)
            {
                fieldBuilder.WithName(passDownName);

                fieldBuilder.WithValue(parameterHelp + " : " + descript + "\n");

                ListOfFieldBuilder.Add(fieldBuilder);
            }

            if(commandWord.Count > 0)
            {
                foreach(CommandWord cWord in commandWord)
                {
                    ListOfFieldBuilder.AddRange(cWord.GetHelpInfoRecur(passDownName));
                    cWord.ClearList();
                }
            }

            return ListOfFieldBuilder;
        }
    }
}
