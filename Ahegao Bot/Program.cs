﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Ahegao_Bot.Services;

namespace Ahegao_Bot
{
    class Program
    {
        private string token = "NDA1MTkwNDYzODc2NzU5NTUz.DUhvkw.A90xwmLPqGrmQLNGzXgMZbtMeHU";
        private Discord.WebSocket.DiscordSocketClient discordClient;

        public static IServiceProvider services;

        public static void Main(string[] args)
            => new Program().MainAsync().GetAwaiter().GetResult();

        public async Task MainAsync()
        {
            discordClient = new Discord.WebSocket.DiscordSocketClient();

            services = ConfigureServices();

            discordClient.Log += services.GetRequiredService<Logger>().Log;
            services.GetRequiredService<Alarm>();
            discordClient.MessageReceived += services.GetRequiredService<CommandHandlingService>().MessageReceived;
            //discordClient.JoinedGuild += services.GetRequiredService<GuildManagement>().NewUser;

            await discordClient.LoginAsync(TokenType.Bot, token);
            await discordClient.StartAsync();

            await Task.Delay(-1);
        }

        private IServiceProvider ConfigureServices()
        {

            return new ServiceCollection()
                // Base
                .AddSingleton(discordClient)
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<Logger>()
                .AddSingleton<Alarm>()
                //.AddSingleton<GuildManagement>()
                // Add additional services here...
                .BuildServiceProvider();
        }
    }
}
